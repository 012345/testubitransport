<?php

namespace App\DataFixtures;

use App\Entity\Grade;
use App\Entity\Student;
use App\Entity\Subject;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class AppFixtures extends Fixture
{
    const SUBJECTS = ["Français", "Anglais", "Mathématiques", "EPS", "Histoire", "Géographie", "Arts plastiques", "Espagnol", "Economie"];

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        for ($i=0; $i<24; $i++) {
            $student = new Student();
            $student->firstName = ($faker->firstName);
            $student->lastName = ($faker->lastName);
            $student->birthday = ($faker->dateTime);

            for ($j=0; $j<$faker->randomDigitNotNull; $j++){
                $grade = new Grade();
                $grade->value = $faker->numberBetween($min=0, $max=20);

                $grade->subject = self::SUBJECTS[$j];
                $grade->student = $student;

                $grade->created_at = new \DateTime("now");
                $manager->persist($grade);
            }

            $student->created_at = new \DateTime("now");
            $manager->persist($student);
        }

        $manager->flush();
    }
}