# TestUBITransport

Create a simple application in order to testing my technical skills 

## Installation

#### Important to read before :


```bash
# Prerequisites
Docker is required. See the official [installation documentation](https://docs.docker.com/get-docker/)..


# This files should contains all project related parameters for your environment (dev, staging, prod ...).
# .env.dist is versioned, don't forget to update it if you need to add a parameter to the application
cp .env.dist .env

```

## Quick Start

```bash
docker-compose up 
```
